package arduino.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JSlider;

import com.fazecast.jSerialComm.SerialPort;

public class CommArduinoTest {

		public static void main(String[] args) {

			JFrame window = new JFrame();
			JSlider slider = new JSlider();
			slider.setMaximum(1023);
			window.add(slider);
			window.pack();
			window.setVisible(true);

			SerialPort[] ports = SerialPort.getCommPorts();
			System.out.println("Select a port:");
			int i = 1;
			for(SerialPort port : ports)
				System.out.println(i++ +  ": " + port.getSystemPortName());
			Scanner s = new Scanner(System.in);
			int chosenPort = s.nextInt();

			SerialPort serialPort = ports[chosenPort - 1];
			if(serialPort.openPort())
				System.out.println("Port opened successfully.");
			else {
				System.out.println("Unable to open the port.");
				return;
			}
			//este es el original *******			
			serialPort.setComPortParameters(9600, 8, 1, SerialPort.NO_PARITY);
			//serialPort.setComPortParameters(9600, 8, 0, SerialPort.NO_PARITY);
			//al cambiar estos valores a 5000 
			serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 2200, 2200);
			//serialPort.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);
			
			Scanner data = new Scanner(serialPort.getInputStream());
			int value = 0;
			String strLinea = null;
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			System.out.println("hora inicio=" + sdf.format(cal.getTime()));
					
			Calendar cal2=null;
			while(data.hasNextLine()){
				cal2 = Calendar.getInstance();				
				strLinea = data.nextLine();
				System.out.println("\n\nhora inicio=" + sdf.format(cal.getTime()));
				System.out.println("hora lectura=" + sdf.format(cal2.getTime()));
				System.out.println("data.nextLine()"+ strLinea);
				try{value = Integer.parseInt(strLinea)*10;}catch(Exception e){}
				slider.setValue(value);
			}
			System.out.println("Done.");
		}

}


