package arduino.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JRadioButton;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import javax.swing.ButtonGroup;

public class PantallaPrincipal implements SerialPortEventListener{

	private JFrame frame;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	//datos de arduino
	public static final int ERROR=-1;
	private OutputStream Output = null;
	private InputStream Input = null;
	SerialPort serialPort;
	//private final String PORT_NAME = "COM3";
	private final String PORT_NAME = "/dev/ttyUSB0";
	private static final int TIME_OUT = 2000;
	private static final int DATA_RATE = 9600;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaPrincipal window = new PantallaPrincipal();
					window.frame.setVisible(true);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 534, 354);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPantallaPrincipal = new JLabel("pantalla principal");
		lblPantallaPrincipal.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 24));
		lblPantallaPrincipal.setBounds(106, 35, 234, 29);
		frame.getContentPane().add(lblPantallaPrincipal);
		
		JLabel lblValorLeido = new JLabel("Valor leido:");
		lblValorLeido.setBounds(32, 115, 107, 21);
		frame.getContentPane().add(lblValorLeido);
		
		JLabel lblResultadohumedad = new JLabel("");
		lblResultadohumedad.setBackground(Color.ORANGE);
		lblResultadohumedad.setOpaque(true);
		lblResultadohumedad.setFont(new Font("Dialog", Font.BOLD, 24));
		lblResultadohumedad.setBounds(180, 118, 113, 41);
		frame.getContentPane().add(lblResultadohumedad);
		
		JButton btnTurnOn = new JButton("Turn ON ");
		btnTurnOn.setBounds(20, 232, 117, 25);
		frame.getContentPane().add(btnTurnOn);
		
		JButton btnTurnOff = new JButton("Turn OFF");
		btnTurnOff.setBounds(170, 232, 117, 25);
		frame.getContentPane().add(btnTurnOff);
		
		JRadioButton rdbtnLedRojo = new JRadioButton("Led ROJO");
		buttonGroup.add(rdbtnLedRojo);
		rdbtnLedRojo.setBounds(70, 200, 149, 23);
		frame.getContentPane().add(rdbtnLedRojo);
		
		JRadioButton rdbtnLedVerde = new JRadioButton("Led VERDE");
		buttonGroup.add(rdbtnLedVerde);
		rdbtnLedVerde.setBounds(230, 200, 149, 23);
		frame.getContentPane().add(rdbtnLedVerde);
		ArduinoConnection();

		
	}
	public void ArduinoConnection() {

		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
	
			if (PORT_NAME.equals(currPortId.getName())) {
			portId = currPortId;
			break;
			}
		}

		if (portId == null) {
			System.exit(ERROR);
			return;
		}

		try {
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
	
			serialPort.setSerialPortParams(DATA_RATE,
			SerialPort.DATABITS_8,
			SerialPort.STOPBITS_1,
			SerialPort.PARITY_NONE);
	
			Output = serialPort.getOutputStream(); //Se prepara a Output //para enviar datos
			Input = serialPort.getInputStream(); //Se prepara input para //recibir datos
	
			serialPort.addEventListener(this); //Se agrega un Event //Listener
			serialPort.notifyOnDataAvailable(true); //Se indica que se //notifique al usuario cuando sea que halla datos disponibles en //el puerto serie
		} catch (Exception e) {
			System.exit(ERROR);
		}


	}
	
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				int datos;
				datos = RecibirDatos(); //Se invoca la función RecibirDatos()
		
				//Esta función devolverá un valor entero en formato ASCII.
				System.out.println((char)datos); //Se imprime en el mensaje
		
				//haciendo la conversión de ASCII a nuestro alfabeto.
			} catch (Exception e) {
				System.err.println(e.toString());
			}
			}
	}
	private int RecibirDatos() throws IOException {
		int Output = 0;
		Output = Input.read();
		return Output;
	}
	
	private void EnviarDatos(String data) {

		try {
		Output.write(data.getBytes());

		} catch (IOException e) {

		System.exit(ERROR);
		}
	}

}
