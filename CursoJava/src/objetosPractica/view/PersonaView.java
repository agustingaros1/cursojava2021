package objetosPractica.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import javafx.scene.control.DialogPane;
import objetosPractica.modelo.Alumno;
import objetosPractica.modelo.Persona;
import objetosPractica.modelo.Profesor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PersonaView {

	private JFrame frame;
	private JTextField textNombre;
	private JTextField textApellido;
	private JTextField textLegajo;
	private JTextField textIosfa;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel lblLegajo;
	private JLabel lblIosfa;
	private JTable table;
	
	private List<Persona> personas;
	private String arrayPersonas[][];
	private Persona personaAmoficiarEliminar;
	
	private JRadioButton rdbtnAlumno;
	private JRadioButton rdbtnProfesor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PersonaView window = new PersonaView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PersonaView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 667, 681);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Personas");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 34));
		lblNewLabel.setBounds(240, 11, 156, 41);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblNombre.setBounds(30, 132, 114, 29);
		frame.getContentPane().add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setFont(new Font("Tahoma", Font.ITALIC, 24));
		textNombre.setBounds(174, 129, 289, 35);
		frame.getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblApellido.setBounds(30, 181, 114, 29);
		frame.getContentPane().add(lblApellido);
		
		textApellido = new JTextField();
		textApellido.setFont(new Font("Tahoma", Font.ITALIC, 24));
		textApellido.setColumns(10);
		textApellido.setBounds(174, 178, 289, 35);
		frame.getContentPane().add(textApellido);
		
		lblLegajo = new JLabel("Legajo");
		lblLegajo.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblLegajo.setBounds(30, 230, 114, 29);
		frame.getContentPane().add(lblLegajo);
		
		textLegajo = new JTextField();
		textLegajo.setFont(new Font("Tahoma", Font.ITALIC, 24));
		textLegajo.setColumns(10);
		textLegajo.setBounds(174, 227, 120, 35);
		frame.getContentPane().add(textLegajo);
		
		lblIosfa = new JLabel("IOSFA");
		lblIosfa.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblIosfa.setBounds(344, 230, 114, 29);
		frame.getContentPane().add(lblIosfa);
		
		textIosfa = new JTextField();
		textIosfa.setFont(new Font("Tahoma", Font.ITALIC, 24));
		textIosfa.setColumns(10);
		textIosfa.setBounds(488, 227, 114, 29);
		frame.getContentPane().add(textIosfa);
		
		rdbtnAlumno = new JRadioButton("Alumno");
		rdbtnAlumno.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//se selecciono alumno hago invisible a 
				// iosfa
				seteoAlumno();
			}
		});
		buttonGroup.add(rdbtnAlumno);
		rdbtnAlumno.setFont(new Font("Tahoma", Font.PLAIN, 24));
		rdbtnAlumno.setBounds(489, 141, 133, 23);
		frame.getContentPane().add(rdbtnAlumno);
		
		rdbtnProfesor = new JRadioButton("Profesor");
		rdbtnProfesor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//se selecciono alumno hago invisible a 
				// legajo
				seteoProfesor();
				
			}
		});
		buttonGroup.add(rdbtnProfesor);
		rdbtnProfesor.setFont(new Font("Tahoma", Font.PLAIN, 24));
		rdbtnProfesor.setBounds(488, 178, 134, 37);
		frame.getContentPane().add(rdbtnProfesor);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdbtnAlumno.isSelected())
					personas.add(new Alumno(textNombre.getText()					, 
											textApellido.getText()					, 
											Integer.parseInt(textLegajo.getText())));
				
				else 
					personas.add(new Profesor(textNombre.getText()					, 
											textApellido.getText()					, 
											textIosfa.getText()));
				llenarGrilla(personas);
				limpiarCampos();
				
			}
		});
		btnAgregar.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnAgregar.setBounds(10, 335, 146, 37);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(personaAmoficiarEliminar==null)
					JOptionPane.showMessageDialog(null, "Debe selerccionar una persona para ser eliminada");
				else {
					personaAmoficiarEliminar.setNombre(textNombre.getText());
					personaAmoficiarEliminar.setApellido(textApellido.getText());

					if(personaAmoficiarEliminar instanceof Alumno)
						((Alumno)personaAmoficiarEliminar).setLegajo(Integer.parseInt(textLegajo.getText()));
					else
						((Profesor)personaAmoficiarEliminar).setIosfa(textIosfa.getText());
					
					personaAmoficiarEliminar = null;
					llenarGrilla(personas);
					limpiarCampos();
			}
			}
		});
		btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnModificar.setBounds(166, 335, 184, 37);
		frame.getContentPane().add(btnModificar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(personaAmoficiarEliminar==null)
					JOptionPane.showMessageDialog(null, "Debe selerccionar una persona para ser eliminada");
				else{
					personas.remove(personaAmoficiarEliminar);
					personaAmoficiarEliminar = null;
				}
				llenarGrilla(personas);
				limpiarCampos();
					
			}
		});
		btnEliminar.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnEliminar.setBounds(360, 335, 117, 37);
		frame.getContentPane().add(btnEliminar);
		
		JButton btnBu = new JButton("Buscar");
		btnBu.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnBu.setBounds(505, 335, 117, 37);
		frame.getContentPane().add(btnBu);
		
		JScrollPane scrollPane = new JScrollPane();
	
		scrollPane.setBounds(44, 452, 578, 179);
		frame.getContentPane().add(scrollPane);
		

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("set presiono la fila =" + table.getSelectedRow());
				 personaAmoficiarEliminar =personas.get(table.getSelectedRow());
				if(personaAmoficiarEliminar instanceof Alumno)
					asignarValores((Alumno)personaAmoficiarEliminar);
				else
					asignarValores((Profesor)personaAmoficiarEliminar);
				
				JOptionPane.showMessageDialog(null, personaAmoficiarEliminar);
			}
		});
	
		scrollPane.setViewportView(table);
		
		JButton btnLimpiarCampos = new JButton("Limpiar Campos");
		btnLimpiarCampos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiarCampos.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnLimpiarCampos.setBounds(199, 404, 239, 37);
		frame.getContentPane().add(btnLimpiarCampos);
		
		//carga valores iniciales
		
		
		llenarGrilla(getCargaInicial());
		//al seleccionar una alumno debo ocultar todo lo relacionado con profesoer
		seteoAlumno();
		//fin de carga inicial
	}
	
	private void llenarGrilla(List<Persona> pPersonas){
		int fila =0 ;
		arrayPersonas = new String[pPersonas.size()][2];
		
		for (Persona persona : pPersonas) {
			for(int col=0;col<2;col++){
				switch (col) {
				case 0:
					arrayPersonas[fila][col] = persona.getNombre();					
					break;
				case 1:
					arrayPersonas[fila][col] = persona.getApellido();					
					break;
				default:
					break;
				}				
			}
			fila++;
		}
		//asigna a la grilla 
		table.setModel(new DefaultTableModel(
				arrayPersonas,
				new String[] {
					"Nombre", "Apellido"
				}
			));
	}
	private void asignarValores(Alumno alu){
		textNombre.setText(alu.getNombre());
		textApellido.setText(alu.getApellido());
		textLegajo.setText(Integer.toString(alu.getLegajo()));
		//realizo el seteo a Alumno
		seteoAlumno();
	}
	private void asignarValores(Profesor prof){
		textNombre.setText(prof.getNombre());
		textApellido.setText(prof.getApellido());
		textIosfa.setText(prof.getIosfa());
		seteoProfesor();
	}	

	private void seteoAlumno(){
		rdbtnAlumno.setSelected(true);
		
		lblLegajo.setVisible(true);
		textLegajo.setVisible(true);
		
		lblIosfa.setVisible(false);
		textIosfa.setVisible(false);
	}

	private void seteoProfesor(){
		rdbtnProfesor.setSelected(true);
		
		lblLegajo.setVisible(false);
		textLegajo.setVisible(false);
		
		lblIosfa.setVisible(true);
		textIosfa.setVisible(true);
	}

	private List<Persona> getCargaInicial(){
		personas = new ArrayList<Persona>();
		personas.add(new Alumno("Juan", "Perez", 1010));
		personas.add(new Profesor("Gabriel", "Casas", "223344/0"));
		personas.add(new Alumno("Roberto", "Carlos", 2020));
		personas.add(new Profesor("Federico", "Weber", "112345/0"));
		return personas;
	}
	
	private void limpiarCampos(){
		textNombre.setText("");
		textApellido.setText("");
		textIosfa.setText("");
		textLegajo.setText("");
		
	}
}
