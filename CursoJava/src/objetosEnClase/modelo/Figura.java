package objetosEnClase.modelo;

/**
 * @author Gabriel
 *Esta clase tiene la funcion de dar la estructura para todas las figuras gemetricas
 */
public abstract class Figura implements Model {
	protected static float maximaSuperficie;
	//atributos
	private String nombre;

	//constructores
	public Figura() {
		nombre ="sin nombre";
	}

	public Figura(String pNombre) {
		super();
		this.nombre = pNombre;
	}

	//getter y setter
	public String getNombre() {					return nombre;			}
	public void setNombre(String pNombre) {		this.nombre = pNombre;	}

	public static float getMaximaSuperficie() {		return maximaSuperficie;	}
	
	//metodos abstractos
	
	/**
	 * Calcula el perimetro de la figura
	 * @return un valor tipo float con el resultado del perimetro
	 */
	public abstract float calcularPerimetro();



	/**
	 * Calcula la superficie de la figura
	 * @return un valor tipo float con el resultado de la superficie
	 */
	public abstract float calcularSuperficie();
	
	/**
	 * Este metodo devuelve los valores armados para ser mostrados dentro de una griila o cualquier elemento grafico
	 * @return
	 */
	public abstract String getValores();
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Figura)) {
			return false;
		}
		//downcast
		Figura other = (Figura) obj;
		if (nombre == null) {
			if (other.getNombre() != null) {
				return false;
			}
		} else if (!nombre.equals(other.getNombre())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "\nnombre=" + nombre ;
	}
	
	
	
	
	
	
	

}
