package pantallas.objetos;

public class Persona {
	//atributos
	private String nombre ;
	private String apellido;
	
	//constructor
	public Persona() {
		super();
	}

	public Persona(String pNombre, String pApellido) {
		super();
		this.nombre = pNombre;
		this.apellido = pApellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String pNombre) {
		this.nombre = pNombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String pApellido) {
		this.apellido = pApellido;
	}

	@Override
	public int hashCode() {
		return nombre.hashCode() + apellido.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Persona per = (Persona) obj;
	
		return nombre!=null 					&&
			   per !=null   					&&
			   nombre.equals(per.getNombre()) 	&&
			   apellido!=null 					&&
			   apellido.equals(per.getApellido())	;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellido=" + apellido + "]";
	}
	
	
	
	
	
	
	

}
