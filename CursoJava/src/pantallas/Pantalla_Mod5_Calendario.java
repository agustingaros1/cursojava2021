package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import util.DateUtil;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.awt.event.ActionEvent;

public class Pantalla_Mod5_Calendario {

	private JFrame frame;
	private JTable table;
	private JTextField textAnio;
	private JComboBox cmbMeses;
	private String diasDelMes[][];
	private String meses[]= {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agostro", "Septiembre", "Octubre", "Noviembre", "Diciembre"} ;
	private String diasDeLaSemana[]= {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "S\u00E1bado", "Domingo"};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_Mod5_Calendario window = new Pantalla_Mod5_Calendario();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_Mod5_Calendario() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 828, 512);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCalendarioPorMes = new JLabel("Calendario por mes");
		lblCalendarioPorMes.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 34));
		lblCalendarioPorMes.setBounds(217, 29, 349, 41);
		frame.getContentPane().add(lblCalendarioPorMes);
		
		cmbMeses = new JComboBox();
		cmbMeses.setFont(new Font("Tahoma", Font.BOLD, 24));
		cmbMeses.setModel(new DefaultComboBoxModel(meses));
		cmbMeses.setBounds(110, 93, 177, 35);
		frame.getContentPane().add(cmbMeses);
		
		JLabel lblMeses = new JLabel("Meses");
		lblMeses.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblMeses.setBounds(10, 93, 73, 29);
		frame.getContentPane().add(lblMeses);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(41, 201, 743, 233);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//table.setFont(new Font("Tahoma", Font.PLAIN, 24));
		scrollPane.setViewportView(table);
		
		JLabel lblAo = new JLabel("A\u00F1o:");
		lblAo.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblAo.setBounds(357, 96, 67, 29);
		frame.getContentPane().add(lblAo);
		
		textAnio = new JTextField();
		textAnio.setText("1972");
		textAnio.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		textAnio.setBounds(434, 94, 100, 33);
		frame.getContentPane().add(textAnio);
		textAnio.setColumns(10);
		
		JButton btnVerCalendario = new JButton("Ver Calendario");
		btnVerCalendario.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		btnVerCalendario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 ;
				Calendar calPrimerDiaDelMes = Calendar.getInstance();
				calPrimerDiaDelMes.set(Integer.parseInt(textAnio.getText()), cmbMeses.getSelectedIndex(), 1);
				
				Calendar calUltimoDiaDelMes = (Calendar)calPrimerDiaDelMes.clone();
				
				int diaInicio = calPrimerDiaDelMes.get(Calendar.DAY_OF_WEEK);
				int ultimoDia = DateUtil.getUltimoDiaDelMes(calUltimoDiaDelMes);
				

				//si es sabado el el ultimo dia es mayor a 30
				//si es domingo y el ultimo dia es  mayor a 29
				
				int iCantFilas =5;
				if(diaInicio == Calendar.SATURDAY && ultimoDia > 30 ||
				   diaInicio == Calendar.SUNDAY  && ultimoDia>29){
					iCantFilas=6;
				}
				diasDelMes = new String[iCantFilas][7];
				//mover a lunes
				Calendar calCalendario = Calendar.getInstance();
				calCalendario = DateUtil.retrocederADia(calPrimerDiaDelMes, Calendar.MONDAY);
				for(int fil=0; fil<iCantFilas;fil++){
					for (int col=0; col<7;col++){
						diasDelMes[fil][col]=Integer.toString(DateUtil.getDay(calCalendario.getTime()));
						calCalendario.add(Calendar.DAY_OF_MONTH, 1);						
					}
				}
						
				
				table.setModel(new DefaultTableModel(
						diasDelMes,
						diasDeLaSemana			
					));

			}
		});
		btnVerCalendario.setBounds(544, 89, 258, 37);
		frame.getContentPane().add(btnVerCalendario);
	}
}
