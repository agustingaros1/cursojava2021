package util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import util.exceptions.ExcelException;







public class ExcelUtil {
	private String nombreArchivo;
	private List<String>encabezado;
	private List<List<String>>filas;
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	public ExcelUtil() {
		super();	
	}
	
	//encabezado
	public ExcelUtil(List<String> encabezado, List<List<String>> filas, String nombreArchivo) {
		this.encabezado = encabezado;
		this.filas = filas;
		this.nombreArchivo = nombreArchivo;
	}
	//getter y setter
	public List<String> getEncabezado() {						return encabezado;				}
	public void setEncabezado(List<String> encabezado) {		this.encabezado = encabezado;	}
	public String getNombreArchivo() {							return nombreArchivo;			}
	public void setNombreArchivo(String nombreArchivo) {		this.nombreArchivo = nombreArchivo;}
	public HSSFWorkbook getWorkbook() {							return workbook;				}
	public void setWorkbook(HSSFWorkbook workbook) {			this.workbook = workbook;		}
	public HSSFSheet getSheet() {								return sheet;					}
	public void setSheet(HSSFSheet sheet) {						this.sheet = sheet;				}
	public  List<List<String>> getFilas() {						return filas;					}
	public void setFilas(List<List<String>> filas) {			this.filas = filas;				}
	
	//agregar elementos
	public void addEncabezado(String parCampoEncabezado){
		
		if(this.encabezado == null)
			this.encabezado = new ArrayList<String>();
		this.encabezado.add(parCampoEncabezado);
	}
	public void addFila(List<String> parFila){
		
		if(this.filas == null)
			this.filas = new ArrayList<List<String>>();
		this.filas.add(parFila);
	}
	
	public void generarExcel() throws ExcelException, IOException{
		
		this.setWorkbook(new HSSFWorkbook());
		this.setSheet(this.getWorkbook().createSheet("Hoja 1"));
		if(!this.validar()){
			throw new ExcelException("El numero de columnas de la cabecera difiere del numero de columnas de las filas");
		}else{
			int numeroFila = 0;
			//creo el encabezado
			HSSFRow filaExcelEncabezado = this.getSheet().createRow(numeroFila);
			this.getSheet().createFreezePane(0, 1);
			for (int i = 0; i < this.getEncabezado().size(); i++) {
				this.addCell(filaExcelEncabezado, i, this.getEncabezado().get(i));
			}
			//creo el contenido del cuerpo
			for (List<String> fila : this.getFilas()) {
				HSSFRow filaExcel = this.getSheet().createRow(++numeroFila);
				for (int j = 0; j < fila.size(); j++) {
					this.addCell(filaExcel, j, fila.get(j));
				}
			}
			//guardo el archivo en la carpeta de archivso preocesados
//			ResourceBundle resources = ResourceBundle.getBundle("resources/Upload");
//		    String pathArchivoProcesado 	= resources.getString("pathArchivoProcesado");
//			FileOutputStream fileOut = new FileOutputStream(pathArchivoProcesado + "\\" + this.getNombreArchivo() + ".xls");
			FileOutputStream fileOut = new FileOutputStream("c:/excel/" + this.getNombreArchivo() + ".xls");
		    this.getWorkbook().write(fileOut);
		    fileOut.close();
		}
	}
	
	private void addCell(HSSFRow row, int posicion, String valorColumna){
		HSSFCell cell = row.createCell(posicion);
		cell.setCellValue(valorColumna);
	}
	
	/*
	 * Validad que la cantidad que columnas del encabezado sea igual a la cantidad de columnas de cada una de las filas.
	 */
	private boolean validar(){
		int columnasEncabezado = this.getEncabezado().size();
		boolean excelValido = true;
		for (List<String> fila : this.getFilas()) {
			int columnasFila = fila.size(); 
			if(columnasFila != columnasEncabezado){
				excelValido = false;
				break;
			}
		}
		return excelValido;
	}
}